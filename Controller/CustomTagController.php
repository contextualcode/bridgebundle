<?php

namespace ThinkCreative\BridgeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CustomTagController extends Controller
{

    public function indexAction($template, $variables) {
        return $this->render(
            $template, $variables
        );
    }

}
