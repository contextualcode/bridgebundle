<?php

namespace ThinkCreative\BridgeBundle\Services;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use eZ\Publish\Core\Repository\Values\Content\Location;
use ThinkCreative\MenubarBundle\Services\Menubar;
use ThinkCreative\MenubarBundle\Classes as MenubarClasses;

class MenubarBridge
{

    protected $Router;
    protected $MenubarService;
    protected $CurrentLocationID;
    protected $SiteLink;

    public function __construct(RouterInterface $router, Menubar $menubar_service, $sitelink = false) {
        $this->Router = $router;
        $this->MenubarService = $menubar_service;
        $this->SiteLink = $sitelink;
    }

    public function configureMenubarHeader(MenubarClasses\MenubarItemComponent $header, Location $menu_root) {
        if(
            is_bool(
                $header->getContent()
            )
        ) {
            $header->setContent(
                $menu_root->contentInfo->name
            );
        }

        $HeaderLink = $header->getLink();
        if(
            !$HeaderLink && $HeaderLink !== false
        ) {
            $header->setLink(
                $this->getLocationPath($menu_root)
            );
        }
    }

    public function createMenubar(OptionsHandler $options) {
        return $this->MenubarService->createMenubar($options);
    }

    public function createMenubarFromOptions(array $options) {
        return $this->MenubarService->createMenubarFromOptions($options);
    }

    public function createMenubarItem(array $configuration, MenubarClasses\Menubar $menubar) {
        return $this->MenubarService->createMenubarItem(
            $configuration, $menubar
        );
    }

    public function createMenubarItemFromLocation(MenubarClasses\Menubar $menubar, Location $location, array $parameters = array()) {
        return $this->MenubarService->createMenubarItem(
            $this->getLocationData( $location, $parameters ), $menubar
        );
    }

    public function getLocationData(Location $location, array $parameters = array()) {
        return array_replace(
            array(
                'content' => $location->contentInfo->name,
                'link' => $this->getLocationPath($location),
                'class' => "location-id-$location->id"  . ( $this->CurrentLocationID && $location->id == $this->CurrentLocationID ? ' current' : '' ),
            ),
            $parameters
        );
    }

    public function getLocationPath(Location $location) {
        if($this->SiteLink) {
            return $this->SiteLink->buildHyperlink(
                $this->SiteLink->generate($location)
            );
        }

        return $this
            ->Router->generate(
                $location, array(), UrlGeneratorInterface::ABSOLUTE_PATH
            )
        ;
    }

    public function setCurrentLocationID($location_id) {
        $this->CurrentLocationID = $location_id;
    }

}
