<?php

namespace ThinkCreative\BridgeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use ThinkCreative\BridgeBundle\DependencyInjection\Compiler\BridgeCompilerPass;

class ThinkCreativeBridgeBundle extends Bundle
{

    protected $name = "ThinkCreativeBridgeBundle";

    public function build(ContainerBuilder $container) {
        parent::build($container);

        $container->addCompilerPass(
            new BridgeCompilerPass()
        );
    }

}
