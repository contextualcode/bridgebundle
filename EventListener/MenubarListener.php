<?php

namespace ThinkCreative\BridgeBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use eZ\Publish\API\Repository\Repository as eZPublishRepository;
use eZ\Publish\API\Repository\Values\Content\Search\SearchResult;
use eZ\Publish\API\Repository\Values\Content\Location;
use Aw\Ezp\FetchBundle\Fetch\Fetcher;
use ThinkCreative\MenubarBundle\Events\MenubarEvents;
use ThinkCreative\MenubarBundle\Events\CreateMenubarEvent;
use ThinkCreative\MenubarBundle\Classes as MenubarClasses;
use ThinkCreative\BridgeBundle\Services\MenubarBridge;

class MenubarListener implements EventSubscriberInterface
{

    protected $MenubarBridgeService;
    protected $eZPublishRepository;
    protected $SearchHandler;

    public function __construct(eZPublishRepository $ezpublish_repository, Fetcher $aw_ezp_fetch, MenubarBridge $menubar_bridge) {
        $this->eZPublishRepository = $ezpublish_repository;
        $this->SearchHandler = $aw_ezp_fetch;
        $this->MenubarBridgeService = $menubar_bridge;
    }

    public function onCreateMenubar(CreateMenubarEvent $event) {
        $OptionsHandler = $event->getOptionsHandler();
        if(
            $OptionsHandler->has('menu_root') && $MenuRootLocationID = $OptionsHandler->get('menu_root')
        ) {
            $LocationService = $this->eZPublishRepository->getLocationService();

            $Menubar = $event->getMenubar();
            $MenuRootLocation = $LocationService->loadLocation($MenuRootLocationID);

            // search system for matching content and process results
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $SearchItems = $this->search(
                $Menubar, $MenuRootLocation, $OptionsHandler->export('*')
            );

            if(
                count($SearchItems) < 1 && $OptionsHandler->get('use_parent')
            ) {
                $MenuRootLocation = $LocationService->loadLocation($MenuRootLocation->parentLocationId);
                $SearchItems = $this->search(
                    $Menubar, $MenuRootLocation, $OptionsHandler->export('*')
                );
            }

            $MenubarItems = array();
            foreach($SearchItems as $Item) {
                $ItemParameters = array();

                $ContentTypeService = $this->eZPublishRepository->getContentTypeService();
                $ContentType = $ContentTypeService->loadContentType(
                    $Item->valueObject->contentInfo->contentTypeId
                );

                if ($ContentType->identifier == 'link') {
                    $ItemParameters['new_window'] = $Item->valueObject->getFieldValue('open_in_new_window')->bool;
                }

                $MenubarItem = $this->MenubarBridgeService->createMenubarItemFromLocation(
                    $Menubar,
                    $ItemLocation = current(
                        $LocationService->loadLocations(
                            $Item->valueObject->contentInfo, $MenuRootLocation
                        )
                    ),
                    $ItemParameters
                );

                $MenuDepth = $OptionsHandler->get('menu_depth');
                if($MenuDepth > 1) {
                    $MenubarItem->setMenu(
                        $this->MenubarBridgeService->createMenubarFromOptions(
                            array(
                                'menu_root' => $ItemLocation->id,
                                'menu_depth' => $MenuDepth - 1,
                                'use_parent' => false,
                            )
                        )
                    );
                }

                $MenubarItems[] = $MenubarItem;
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            // reconfigure menubar header if neccesary
            if(
                $Menubar->hasHeader(true)
            ) {
                $this->MenubarBridgeService->configureMenubarHeader(
                    $Menubar->getHeader(), $MenuRootLocation
                );
            }

            $IncludeItems = $OptionsHandler->get('include');
            foreach($OptionsHandler->get('append') as $Item) {
                $IncludeItems[] = array_replace(
                    $Item,
                    array(
                        // ensure item is appended to the menu
                        'placement' => false,
                    )
                );
            }

            // handle the inclusion of the menu root location
            if(
                $IncludeMenuRoot = $OptionsHandler->get('include_root')
            ) {
                $IncludeItems[] = $this->MenubarBridgeService->getLocationData(
                    $MenuRootLocation,
                    array(
                        'placement' => ($IncludeMenuRoot === true || $IncludeMenuRoot == 'prepend') ? 1 : false
                    )
                );
            }

            if($IncludeItems) {
                $Count = count($MenubarItems);
                foreach($IncludeItems as $Item) {
                    // an object spliced into the array must be place into an array in order to function as intended
                    // http://us3.php.net/manual/en/function.array-splice.php
                    array_splice(
                        $MenubarItems,
                        (isset($Item['placement']) && $Item['placement']) ? --$Item['placement'] : $Count,
                        0,
                        array(
                            $this->MenubarBridgeService->createMenubarItem($Item, $Menubar)
                        )
                    );
                    $Count++;
                }
            }

            $Menubar->setItems($MenubarItems);
        }
    }

    public function onController(FilterControllerEvent $event) {
        $KernelRequestAttributes = $event->getRequest()->attributes;
        if(
            $KernelRequestAttributes->has('locationId')
        ) {
            $this->MenubarBridgeService->setCurrentLocationID(
                $KernelRequestAttributes->get('locationId')
            );
        }
    }

    public static function getSubscribedEvents() {
        return array(
            KernelEvents::CONTROLLER => array('onController'),
            MenubarEvents::CREATE_MENUBAR => array('onCreateMenubar'),
        );
    }

    protected function search($menubar, $location, $options) {
        $LocationService = $this->eZPublishRepository->getLocationService();

        // reconfigure filter options to include the menu root location
        $options->set(
            'filter',
            array(
                'AND' => array_merge(
                    array(
                        array(
                            'parent_location_id' => array(
                                'EQ' => $location->id
                            )
                        ),
                        array(
                            'visibility' => array(
                                'EQ' => true
                            )
                        ),
                    ),
                    $options->get('filter')
                )
            )
        );

        $SearchResults = $this->SearchHandler->fetch(
            $options
                ->export(
                    array('filter', 'sort', 'limit')
                )
                ->all()
        );

        return (
            $SearchResults && $SearchResults instanceof SearchResult ? $SearchResults->searchHits : array()
        );
    }

}
