<?php

namespace ThinkCreative\BridgeBundle\Services;

use eZ\Publish\Core\FieldType\XmlText\Converter;
use eZ\Publish\Core\FieldType\XmlText\Converter\Html5 as eZXMLTextConverter;
use ThinkCreative\BridgeBundle\Services\CustomTagsManager;

class eZXMLTextPreConverter implements Converter
{

    protected $CustomTagsManager;
    protected $eZXMLTextConverter;

    public function __construct(CustomTagsManager $customtags_manager, eZXMLTextConverter $ezxmltext_converter) {
        $this->CustomTagsManager = $customtags_manager;
        $this->eZXMLTextConverter = $ezxmltext_converter;
    }

    public function convert(\DOMDocument $xml) {
        $XPath = new \DOMXPath($xml);

        foreach(
            $XPath->query('//custom[not(ancestor::custom)]') as $Custom
        ) {
            $CustomTagName = $Custom->getAttribute('name');

            if(
                $this->CustomTagsManager->hasRegisteredItem($CustomTagName)
            ) {
                $CustomTag = $this->CustomTagsManager->createCustomTag(
                    $CustomTagName, $this->getCustomAttributes($Custom), $this->convertCustomTagContent($xml, $Custom)
                );

                if(
                    $CustomTagOutput = $this->CustomTagsManager->renderCustomTag($CustomTag)
                ) {
                    while(
                        $Custom->hasChildNodes()
                    ) {
                        $Custom->removeChild($Custom->firstChild);
                    }

                    $Custom->appendChild(
                        $xml->createCDATASection($CustomTagOutput)
                    );
                }

            }

        }
    }

    protected function convertCustomTagContent(\DOMDocument $xml, \DOMElement $element) {
        if(
            $element->hasChildNodes()
        ) {
            $DOMDocument = new \DOMDocument(
                $xml->xmlVersion, $xml->xmlEncoding
            );
            $DOMDocument->loadXML('<root></root>');
            $DOMDocument->replaceChild(
                $DOMDocument->importNode($xml->documentElement), $DOMDocument->documentElement
            );

            foreach(
                $element->childNodes as $ChildNode
            ) {
                $DOMDocument->documentElement->appendChild(
                    $DOMDocument->importNode(
                        $ChildNode, true
                    )
                );
            }

            return $this->eZXMLTextConverter->convert($DOMDocument);
        }
    }

    protected function getCustomAttributes(\DOMElement $element) {
        if(
            $Attributes = (array) simplexml_import_dom($element)->attributes("http://ez.no/namespaces/ezpublish3/custom/")
        ) {
            return current($Attributes);
        }
        return array();
    }

}
