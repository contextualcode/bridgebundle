<?php

namespace ThinkCreative\BridgeBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use eZ\Publish\Core\MVC\Symfony\MVCEvents as eZPublishEvents;
use eZ\Publish\Core\MVC\Symfony\Event\PreContentViewEvent;

class BridgeListener implements EventSubscriberInterface
{

    protected $Container;
    protected $RequestViewHandler;

    public function __construct(ContainerInterface $container) {
        $this->Container = $container;
        $this->RequestViewHandler = $container->get('thinkcreative.requestviewhandler');
    }

    public function onPreContentView(PreContentViewEvent $event) {
        if(
            $this->RequestViewHandler->isMasterRequest()
        ) {
            $this->RequestViewHandler->processContentView(
                $this->Container, $event->getContentView()
            );
        }
    }

    public static function getSubscribedEvents() {
        return array(
            eZPublishEvents::PRE_CONTENT_VIEW => array('onPreContentView'),
        );
    }

}
